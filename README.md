## Ejemplo de uso

```c

void on_tec1_press(void *noUsado){
	uartWriteString(UART_USB, "Tecla 1 Presionada");
	// Habilito todas las interrupciones de TEC1
	gpioInterrupt(TEC1, true);
}

void on_tec1_release(void *noUsado){
	uartWriteString(UART_USB, "Tecla 1 Soltada");
	// Habilito todas las interrupciones de TEC1
	gpioInterrupt(TEC1, true);
}

void on_tec2_press(void *noUsado){
	uartWriteString(UART_USB, "Tecla 2 Presionada");
	// Habilito todas las interrupciones de TEC2
	gpioInterrupt(TEC2, true);
}

void on_tec2_release(void *noUsado){
	uartWriteString(UART_USB, "Tecla 2 Soltada");
	// Habilito todas las interrupciones de TEC2
	gpioInterrupt(TEC2, true);
}

void on_tec3_press(void *noUsado){
	uartWriteString(UART_USB, "Tecla 3 Presionada");
	// Habilito todas las interrupciones de TEC3
	gpioInterrupt(TEC3, true);
}

void on_tec3_release(void *noUsado){
	uartWriteString(UART_USB, "Tecla 3 Soltada");
	// Habilito todas las interrupciones de TEC3
	gpioInterrupt(TEC3, true);
}

void on_tec4_press(void *noUsado){
	uartWriteString(UART_USB, "Tecla 4 Presionada");
	// Habilito todas las interrupciones de TEC4
	gpioInterrupt(TEC4, true);
}

int main(void) {
	// ----- Setup -----------------------------------
	boardInit();
	uartInit(UART_USB, 115200);

	// Seteo un callback al evento de flanco descendente y habilito su interrupcion
	gpioCallbackSet(TEC1, INT_FALL, on_tec1_press, NULL);
	// Seteo un callback al evento de flanco ascendente y habilito su interrupcion
	gpioCallbackSet(TEC1, INT_RISE, on_tec1_release, NULL);
	// Habilito todas las interrupciones de TEC1
	gpioInterrupt(TEC1, true);
	// Seteo un callback al evento de flanco descendente y habilito su interrupcion
	gpioCallbackSet(TEC2, INT_FALL, on_tec2_press, NULL);
	// Seteo un callback al evento de flanco ascendente y habilito su interrupcion
	gpioCallbackSet(TEC2, INT_RISE, on_tec2_release, NULL);
	// Habilito todas las interrupciones de TEC2
	gpioInterrupt(TEC2, true);
	// Seteo un callback al evento de flanco descendente y habilito su interrupcion
	gpioCallbackSet(TEC3, INT_FALL, on_tec3_press, NULL);
	// Seteo un callback al evento de flanco ascendente y habilito su interrupcion
	gpioCallbackSet(TEC3, INT_RISE, on_tec3_release, NULL);
	// Habilito todas las interrupciones de TEC3
	gpioInterrupt(TEC3, true);
	// Seteo un callback al evento de flanco descendente y habilito su interrupcion
	gpioCallbackSet(TEC4, INT_FALL, on_tec4_press, NULL);
	// Habilito todas las interrupciones de TEC4
	gpioInterrupt(TEC4, true);

	// ----- Repeat for ever -------------------------
	while ( true) {
		gpioToggle(LED);
		delay(500);
	}

	// YOU NEVER REACH HERE, because this program runs directly or on a
	// microcontroller and is not called by any Operating System, as in the
	// case of a PC program.
	return 0;
}
```