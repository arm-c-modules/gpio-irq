/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/06/09
 *===========================================================================*/

/*=====[Avoid multiple inclusion - begin]====================================*/

#ifndef _GPIO_IRQ_H_
#define _GPIO_IRQ_H_

/*=====[Inclusions of public function dependencies]==========================*/

#include "sapi.h"

/*=====[C++ - begin]=========================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*=====[Definition macros of public constants]===============================*/

/*=====[Public function-like macros]=========================================*/

/*=====[Definitions of public data types]====================================*/

typedef enum {
	INT_FALL, INT_RISE, INT_LOW, INT_HIGH,
} gpioEvents_t;

typedef struct {
	callBackFuncPtr_t isrCallbackGPIO;
	void *isrCallbackGPIOParams;
	gpioEvents_t event;
	gpioMap_t gpio;
} irqChannels_t;

/*=====[Prototypes (declarations) of public functions]=======================*/

void gpioCallbackSet(gpioMap_t gpio, gpioEvents_t event,
		callBackFuncPtr_t callbackFunc, void* callbackParam);

void gpioInterrupt(gpioMap_t gpio, bool_t enable);

/*=====[Prototypes (declarations) of public interrupt functions]=============*/

void GPIO0_IRQHandler(void);

void GPIO1_IRQHandler(void);

void GPIO2_IRQHandler(void);

void GPIO3_IRQHandler(void);

void GPIO4_IRQHandler(void);

void GPIO5_IRQHandler(void);

void GPIO6_IRQHandler(void);

void GPIO7_IRQHandler(void);

/*=====[C++ - end]===========================================================*/

#ifdef __cplusplus
}
#endif

/*=====[Avoid multiple inclusion - end]======================================*/

#endif /* _GPIO_IRQ_H_ */

